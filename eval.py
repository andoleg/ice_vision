import os
import pickle
import subprocess
import argparse

import numpy as np

from utils import combine_boxes, combine_labels


def evaluate(scorer_path, groundtruth_path, solution_path):
    output = subprocess.check_output([scorer_path, groundtruth_path, solution_path]).decode()
    lines = output.split('\n')
    assert lines[0].startswith('Total score:')
    total_score = float(lines[0].split('\t')[1])
    total_penalty = float(lines[1].split('\t')[1])

    return total_score, total_penalty


def main():
    parser = argparse.ArgumentParser('eval')
    parser.add_argument('--pkl_path', type=str, help='Path to predictions')
    parser.add_argument('--tsv_path', type=str, help='Where to save tsv')
    parser.add_argument('--gt_path', type=str, help='Path to groundtruth folder')
    parser.add_argument('--scorer_path', type=str, help='Path to scorer executable')
    args = parser.parse_args()

    with open(args.pkl_path, 'rb') as f:
        img_name_to_predictions = pickle.load(f)

    img_name_to_predictions = {
    img_name.replace('/left', '_left').replace('/right', '_right').replace('.jpg', ''): predictions for
    img_name, predictions in img_name_to_predictions.items()}

    content = sorted(img_name_to_predictions)

    img_name_to_bboxes_with_predictions_list = {}
    for img_name in content:
        predictions = img_name_to_predictions[img_name]
        bboxes, predictions_list = combine_boxes(predictions)
        predictions_list = combine_labels(predictions_list)
        img_name_to_bboxes_with_predictions_list[img_name] = (bboxes, predictions_list)


    thresholds = np.arange(0.5, 1.5, 0.1)

    threshold_triples = []
    for i in range(len(thresholds)):
        for j in range(i, len(thresholds)):
            for k in range(j, len(thresholds)):
                threshold_triples.append((thresholds[i], thresholds[j], thresholds[k]))

    threshold_triples = np.array(threshold_triples, dtype=np.float32)

    total_scores = []
    for thresh_1, thresh_2, thresh_3 in threshold_triples:
        content = sorted(img_name_to_bboxes_with_predictions_list)

        with open(args.tsv_path, 'w') as f:
            f.write('frame\txtl\tytl\txbr\tybr\tclass\n')
            for img_name in content:
                bboxes, predictions_list = img_name_to_bboxes_with_predictions_list[img_name]

                for bbox, predictions in zip(bboxes, predictions_list):
                    xmin, ymin, xmax, ymax = bbox
                    if predictions[0][1] > thresh_3:
                        category = predictions[0][0]
                    elif predictions[1][1] > thresh_2:
                        category = predictions[1][0]
                    elif predictions[2][1] > thresh_1:
                        category = predictions[2][0]
                    else:
                        continue

                    basename = os.path.basename(img_name)
                    img_name = img_name.replace(basename, basename.zfill(6)).replace('/left', '_left').replace('/right',
                                                                                                               '_right')
                    f.write('\t'.join([img_name, str(xmin), str(ymin), str(xmax), str(ymax), category]) + '\n')

        total_score, _ = evaluate(args.scorer_path, args.gt_path, args.tsv_path)
        total_scores.append(total_score)

    total_scores = np.array(total_scores)
    best_idx = total_scores.argmax()
    print(threshold_triples[best_idx], total_scores[best_idx])

if __name__ == '__main__':
    main()
