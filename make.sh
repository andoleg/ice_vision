#!/usr/bin/env bash

pip install -r requirements.in

cd mmcv
python setup.py develop
cd ..

cd mmdetection
./compile.sh
python setup.py develop
cd ..