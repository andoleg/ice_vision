import numpy as np
import torch
from mmdet.apis import init_detector


class Detector(object):

    def __init__(self, cfg_path, ckpt_path, label_to_category, thresh):
        self.model = init_detector(cfg_path, ckpt_path, device='cuda:0')
        self.label_to_category = label_to_category
        self.thresh = thresh

    def detect(self, img):
        with torch.no_grad():
            predictions = self.model(return_loss=False, rescale=True, **img)

        scores = []
        bboxes = []
        categories = []

        for i, dets in enumerate(predictions):
            if len(dets) > 0:
                dets = dets[dets[:, 4] > self.thresh]
                for det in dets:
                    xmin, ymin, xmax, ymax, score = det
                    bboxes.append((xmin, ymin, xmax, ymax))
                    scores.append(score)
                    categories.append(self.label_to_category[i + 1])

        scores = np.array(scores, dtype=np.float32)
        bboxes = np.array(bboxes, dtype=np.float32)

        return bboxes, scores, categories
