import os
import json
import yaml

from detector import Detector


class Predictor(object):

    def __init__(self, config):
        det_cfg_path = config['DETECTOR']['CONFIG_PATH']
        det_ckpt_path =config['DETECTOR']['WEIGHTS_PATH']

        with open(config['DETECTOR']['LABEL_MAP_PATH'], 'r') as f:
            category_to_label = json.load(f)
        label_to_category = {label: category for category, label in category_to_label.items()}

        self._detector = Detector(det_cfg_path,
                                  det_ckpt_path,
                                  label_to_category,
                                  config['DETECTOR']['THRESHOLD'])

    def predict(self, img):
        return self._detector.detect(img)