import json
from pathlib import Path

import cv2
from tqdm import tqdm
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser("Convert ice_vision annotations to COCO format")
parser.add_argument("--keep-classes", action="store_true")
args = parser.parse_args()

ignore_classes = 'N/A'

data_root = Path('data')
data_path = data_root / 'ice_vision'
annotations_path = data_path / 'annotations'

with (annotations_path / 'label_map.json').open() as f:
    label_map = json.load(f)

default_signs = ['2.1', '2.4', '3.1', '3.24', '3.27', '4.1', '4.2', '5.19', '5.20', '8.22']

for set_name in ['training', 'test', 'final']:
    counts = defaultdict(int)

    images_folder = data_path / set_name
    for img_path in tqdm(list(images_folder.rglob('*.jpg')), desc=set_name):
        parts = img_path.relative_to(data_path).with_suffix('.tsv').parts
        ann_path = annotations_path / parts[0] / f"{parts[1]}_{parts[2]}" / parts[3]
        with ann_path.open() as f:
            lines = [x.strip() for x in f.readlines()]
        for line in lines[1:]:
            cls, *rest = line.split('\t')
            for s in default_signs:
                if cls.startswith(s):
                    cls = s
            if cls in 'N/A':
                continue
            counts[cls] += 1
    print(set_name)
    print(sorted(counts.items(), key=lambda x: x[1]))
    print(len(counts))
    print(f"Total signs: {sum(counts.values())}")
    print(f"Chosen signs: {sum([v for k,v in counts.items() if k in default_signs])}")
