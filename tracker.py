import pickle
from collections import defaultdict

import numpy as np

from iou_tracker import track_iou, inter_track
from utils import combine_boxes, combine_labels


def _preds_to_video(preds):
    preds = {img_name.replace('/left', '_left').replace('/right', '_right').rsplit('.', 1)[0]: predictions for
             img_name, predictions in preds.items()}

    video_frames = defaultdict(list)

    for img_name, predictions in preds.items():
        video_name, frame_idx = img_name.split('/')
        frame_idx = int(frame_idx)
        if frame_idx % 3 > 0:
            continue
        video_frames[video_name].append((frame_idx, predictions))

    return video_frames


def repeat_predictions(preds, real_num_frames):
    video_frames = _preds_to_video(preds)

    result = {}
    for video_name, video_preds in video_frames.items():
        video_preds = sorted(video_preds, key=lambda x: x[0])
        current_dets = None
        current_idx = video_preds[0][0] - 1
        while current_idx < real_num_frames:
            current_idx += 1
            if len(video_preds) > 0 and current_idx == video_preds[0][0]:
                current_dets = video_preds.pop(0)
            result[video_name + '/' + format(current_idx, "06d")] = current_dets[1]
    return result


def _combine_labels(labels):
    def combine_label(label):
        return max(label, key=lambda x: x[1])

    label0 = []
    label1 = []
    label2 = []

    for lbl in labels:
        label0.append(lbl[0])
        label1.append(lbl[1])
        label2.append(lbl[2])
    return combine_label(label0), combine_label(label1), combine_label(label2)


def track(preds, sigma_l=0, sigma_h=0.65, sigma_iou=0.3, t_min=1):
    video_frames = _preds_to_video(preds)

    result = defaultdict(lambda: ([], []))
    for video_name, video_preds in video_frames.items():
        video_dets = []
        video_boxes_to_labels = []
        frame_idxs = []
        video_preds = sorted(video_preds, key=lambda x: x[0])
        for frame_idx, frame_preds in video_preds:
            frame_idxs.append(frame_idx)
            boxes, labels = combine_boxes(frame_preds)
            labels = combine_labels(labels)
            video_boxes_to_labels.append({tuple(box): lbls for box, lbls in zip(boxes, labels)})
            video_dets.append([{'score': lbl[-1][1], 'bbox': box} for box, lbl in zip(boxes, labels)])
        tracks = track_iou(video_dets, sigma_l, sigma_h, sigma_iou, t_min)

        video_name = '/'.join(video_name.rsplit('_', 1))
        for track in tracks:
            boxes = np.array(track['bboxes'])
            num_boxes = len(boxes)
            start_frame = track['start_frame'] - 1

            labels = []
            for idx, box in enumerate(boxes):
                labels.append(video_boxes_to_labels[start_frame + idx][tuple(box)])
            label = _combine_labels(labels)
            inter_boxes, track_frame_idxs = inter_track(boxes,
                                                        [frame_idxs[start_frame + idx] for idx in range(num_boxes)])
            for box, frame_idx in zip(inter_boxes, track_frame_idxs):
                frame_results = result[video_name + '/' + format(frame_idx, "06d")]
                frame_results[0].append(box)
                frame_results[1].append(label)
    return dict(result)


if __name__ == '__main__':
    with open('final_frame_predictions_all.pkl', 'rb') as f:
        preds = pickle.load(f)
    result = track(preds)
    with open('frames_tracked_2_skip.pkl', 'wb') as f:
        pickle.dump(result, f)
